from setuptools import setup, find_namespace_packages

setup(
    name="locus",
    version="0.0.1",
    package_dir={"": "src"},
    packages=find_namespace_packages(where="src"),
    python_requires=">=3.8",
    install_requires=["numpy"],
    test_requires=["pytest", "pytest-cov"],
    author="Mayank Singh",
    author_email="myan2007@gmail.com",
    description="Locus CI Package",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
