**How to Setup CI ?**

Protect your master branch - https://docs.github.com/en/enterprise/2.16/admin/developer-workflow/configuring-protected-branches-and-required-status-checks

(If gitlab protect master branch - https://docs.gitlab.com/ee/user/project/protected_branches.html)

* This will protect master branch from direct push and only merging is allowed by maintainers

In GitHub, add below files which you can find in the submission
```
.gitlab-ci.yml - to configure GitLab CI/CD.
pylint-exit.py - pylint-exit is a small command-line utility that can be used to re-process the pylint return code and translate it into a scripting-friendly return code
pytest.ini - test configs
requirements.txt - required packages for this project
setup.py - package setup
src - code directory
tests - test cases
.coveregec - code covereges
```

* In GitLab, push the whole code and it will start working

Add aws credentials in Gitlab:-

1. Go to Settings > CI/CD.
2. Expand the Variables section.
3. Add Variables and its values
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
4. Next to the variable you want to protect, click Edit.
5. Select the Mask variable check box.
6. Click Update variable