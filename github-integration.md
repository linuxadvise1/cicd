**Connect with Personal Access Token**
Note: Personal access tokens can only be used to connect GitHub.com repositories to GitLab, and the GitHub user must have the owner role.
To perform a one-off authorization with GitHub to grant GitLab access your repositories:

1. Open https://github.com/settings/tokens/new to create a Personal Access Token. This token will be used to access your repository and push commit statuses to GitHub.

2. The repo and admin:repo_hook should be enable to allow GitLab access to your project, update commit statuses, and create a web hook to notify GitLab of new commits.

3. In GitLab, go to the new project page, select the CI/CD for external repo tab, and then click GitHub.

4. Paste the token into the Personal access token field and click List Repositories. Click Connect to select the repository.

**GitLab will:**
- Import the project
- Enable Pull Mirroring
- Enable GitHub project integration
- Create a web hook on GitHub to notify GitLab of new commits


* For Gitlab just create a new repo and push the whole code